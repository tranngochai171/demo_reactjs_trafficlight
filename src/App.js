import React from 'react';
import './App.css';
import Accordion from './components/demoChildren/Accordion';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './components/ToDoList/Home'
import TrafficLight from './components/TrafficLight/TrafficLight'
function App() {
  return (
    <Router>
      <div className="App">
        <nav>
          <ul>
            <li><Link to="/home">Home todo-list</Link></li>
            <li><Link to="/traffic-ligh">Traffic light</Link></li>
            <li><Link to="/accordion">Accordion</Link></li>
          </ul>
        </nav>
        <Route path="/home" exact ><Home /></Route>
        <Route path="/traffic-light"><TrafficLight /></Route>
        <Route path="/accordion" ><Accordion heading="Heading">Topy sao pro qua vay</Accordion></Route>
      </div> 
    </Router>
  );
}

export default App;
