import React, { Component } from 'react'
import CONSTANT from '../constant.json'
import './TrafficLight.css'
class TrafficLight extends Component {
  constructor(props) {
    super(props)
    this.state = this.resetState()
  }

  resetState = (currentIndex = 0, mode = true, intervalId = '') => {
    let count = CONSTANT.COUNT[CONSTANT.COLORS[currentIndex]]
    return {
      currentIndex,
      count,
      mode, // true: auto, false: manual
      intervalId
    }
  }

  // red: 15s, yellow: 3s, green: 10s
  componentDidMount() {
    let intervalId = this.startAutoMode()
    this.setState({
      intervalId
    })
  }

  componentWillUnmount() { // clear any actions to avoid memory leaks
    if (this.state.intervalId) {
      clearInterval(this.state.intervalId)
    }
  }

  startAutoMode = () => {
    return setInterval(() => {
      let {currentIndex, count} = this.state;
      let currentColor = CONSTANT.COLORS[currentIndex];
      let changeColor = false;
      if(--count <= 0) {
        changeColor = true;
        switch(currentColor) {
          case CONSTANT.INDIVIDUAL_COLORS.RED:
            count = CONSTANT.COUNT.GREEN
            break;
          case CONSTANT.INDIVIDUAL_COLORS.YELLOW:
            count = CONSTANT.COUNT.RED
            break;
          default:
            count = CONSTANT.COUNT.YELLOW
            break;
        }
      }
      if (changeColor) {
        currentIndex = (currentIndex === CONSTANT.COLORS.length - 1) ? 0 : currentIndex + 1;
      }
      this.setState({
        currentIndex,
        count
      })
    }, 1000);
  }

  changeMode = () => {
    let {currentIndex, intervalId, mode} = this.state
    mode = !mode
    if(!mode) {
      clearInterval(intervalId)
    } else {
      intervalId = this.startAutoMode()
    }
    this.setState(this.resetState(currentIndex, mode, intervalId))
  }

  selectColor = (color) => {
    if (!this.state.mode) {
      this.setState({
        currentIndex: CONSTANT.COLORS.indexOf(color)
      })
    }
  }

  render() {
    const {currentIndex, count, mode} = this.state
    const currentColor = CONSTANT.COLORS[currentIndex]
    return (
      <div className='TrafficLight'>
        <div onClick={() => this.selectColor(CONSTANT.INDIVIDUAL_COLORS.RED)} className={`bulb red ${currentColor === CONSTANT.INDIVIDUAL_COLORS.RED ? 'active' : ''}`}></div>
        <div onClick={() => this.selectColor(CONSTANT.INDIVIDUAL_COLORS.YELLOW)} className={`bulb yellow ${currentColor === CONSTANT.INDIVIDUAL_COLORS.YELLOW ? 'active' : ''}`}></div>
        <div onClick={() => this.selectColor(CONSTANT.INDIVIDUAL_COLORS.GREEN)} className={`bulb green ${currentColor === CONSTANT.INDIVIDUAL_COLORS.GREEN ? 'active' : ''}`}></div>
        <div>{`${mode ? 'Auto' : 'Manual'} - ${count}`}</div>
        <button onClick={() => this.changeMode()}>Change Mode</button>
      </div>
    )
  }
}

export default TrafficLight;