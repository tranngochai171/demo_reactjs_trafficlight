import React, { Component } from 'react'
import ToDoItem from './TodoItem';
import CONSTANT from '../constant.json'
const shortId = require('shortid')
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentFilter: CONSTANT.STATUS.ALL,
      newTaskTitle: '',
      data: []
    }
  }

  changeStatus = (currentFilter) => {
    this.state.currentFilter !== currentFilter && this.setState({ currentFilter })
  }

  renderData = () => {
    const {currentFilter, data} = this.state
    let dataRender = JSON.parse(JSON.stringify(data))
    switch(currentFilter) {
      case CONSTANT.STATUS.ACTIVE:
        dataRender = dataRender.filter(item => !item.isComplete)
        break;
      case CONSTANT.STATUS.COMPLETE:
        dataRender = dataRender.filter(item => item.isComplete)
        break;
      default:
    }
    return dataRender.map((item, index) => <ToDoItem deleteTaskById={this.deleteTaskById} toogleProgress={this.toogleProgress} key={index} data={item}/>)
  }
  
  toogleProgress = (id) => {
    const data = [...this.state.data]
    const target = data.find((item) => item.id === id)
    target.isComplete = !target.isComplete
    if (target) {
      this.setState({
        data
      })
    }
  }

  deleteTaskById = (id) => {
    const data = [...this.state.data]
    const index = data.findIndex(item => item.id === id)
    if(index > -1) {
      data.splice(index, 1)
      this.setState({
        data
      })
    }
  }

  getNewTask = (title) => {
    return {
      id: shortId.generate(),
      title,
      isComplete: false
    }
  }

  onChangeNewTask = (event) => {
    this.setState({
      newTaskTitle: event.target.value
    })
  }

  pressEnter = (event) => {
    let {newTaskTitle, data} = this.state
    if (newTaskTitle && event.key === 'Enter') {
      data.push(this.getNewTask(newTaskTitle))
      this.setState({
        data,
        newTaskTitle: ''
      })
    }
  }

  countTask = (isComplete) => {
    const taskCount = isComplete ? this.state.data.filter(item => item.isComplete) : this.state.data.filter(item => !item.isComplete)
    return taskCount.length
  }

  clearCompleteTasks = () => {
    this.setState({
      data: this.state.data.filter(item => !item.isComplete)
    })
  }

  render() {
    const {currentFilter, newTaskTitle} = this.state
    return (
      <div>
        <div>
          <input className="mb-5" value={newTaskTitle} onKeyDown={this.pressEnter} onChange={event => this.onChangeNewTask(event)}></input>
        </div>
        { this.state.data?.length > 0 ?
            (
              <>
                {this.renderData()}
                <div className="container">
                  <div className="row m-5">
                    <div className="col-4">{`${this.countTask(false /* uncomplete tasks */)} item${this.countTask() > 1 ? 's' : ''} left`}</div>
                    <div className="col-4">
                      <div className="row">
                        { CONSTANT.STATUS_LIST.map((item, index) => <div onClick={() => this.changeStatus(item)} key={index} className={`col-4 ToDoList-status ${currentFilter === item ? 'status-active' : ''}`}>{item}</div>) }
                        {/* <div onClick={() => this.changeStatus(CONSTANT.STATUS.ALL)} className="col-4 ToDoList-status">all</div>
                        <div onClick={() => this.changeStatus(CONSTANT.STATUS.ACTIVE)} className="col-4 ToDoList-status">Active</div>
                        <div onClick={() => this.changeStatus(CONSTANT.STATUS.COMPLETE)} className="col-4 ToDoList-status">Complete</div> */}
                      </div>
                    </div>
                      <div className="col-4">{this.countTask(true /* complete tasks */) > 0 && <button onClick={this.clearCompleteTasks} className="btn btn-warning">Clear completed</button>}</div>
                  </div>
                </div>
              </>
            )
            :
            <h1>The list is empty</h1>
        }
        
      </div>
    )
  }
}
