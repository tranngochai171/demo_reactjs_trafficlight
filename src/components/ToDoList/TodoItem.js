import React, { Component } from "react";
import "./ToDoItem.css";
import PropTypes from 'prop-types'

class ToDoItem extends Component {
  render() {
    const { data, toogleProgress, deleteTaskById } = this.props;
    return (
      <div className={`ToDoItem ${data?.isComplete ? 'ToDoItem-complete' : ''}`}>
        <div className="row">
          <p className="col-8" onClick={() => toogleProgress(data.id)}>{ data.title }</p>
          <button onClick={() => deleteTaskById(data.id)} className="btn btn-danger col-2">Delete</button>
        </div>

      </div>
    );
  }
}

ToDoItem.propTypes = {
  deleteTaskById: PropTypes.func,
  toogleProgress: PropTypes.func,
  data: PropTypes.shape({
    isComplete: PropTypes.bool,
    title: PropTypes.string,
    id: PropTypes.number
  }
  )
}

export default ToDoItem;
